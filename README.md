# let's breathe
This is a very basic animation that you can watch to help time your inhales and exhales when doing a breathing exercise. 


## built with Glitch 🎏✨

[Glitch](https://glitch.com) is a friendly community where millions of people come together to build web apps and websites.

Need more help? [Check out our Help Center](https://help.glitch.com/) for answers to any common questions.